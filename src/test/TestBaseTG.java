package test;

import java.io.IOException;
import java.util.List;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

//import pages.TestPage;
import pages.*;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.font.PDFont;

import static org.testng.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import test.ManipularExcel;

	public class TestBaseTG extends BasePage {

	protected WebDriver driver;
	protected  WebDriverWait wait;
	String usuario ;
	String paswd ; 
	

	/*
	int Loop = ManipularExcel.MAXIMO_FILAS;
	@DataProvider(name = "filas")
    private Object[] createData1() {
	 ArrayList<String> filas = new ArrayList<>();
		for(int i=3; i<= Loop; i++) {
			String C = String.valueOf(i);
			filas.add(C);
		}
	       return filas.toArray();
    }
    */  

	@BeforeClass
	public void IngresoAmbiente() {
		this.driver = setConexion.setup();
		wait = new WebDriverWait(driver, 10);
		TestBase TB = new TestBase(driver);
		TB.UrlDeLaWeb(apuntaA);
		
		
	}	
	
	@AfterMethod
	public void IrHome(ITestResult result) {
		
		if(ITestResult.FAILURE==result.getStatus()) 
		{ 
			BasePage.captureScreenshot(driver, BasePage.base__fechaActual("kk_mm_ss")); 
		}		 
		TestBase TB = new TestBase(driver); 
		TB.UrlDeLaWeb(apuntaA);
	}
	
	@AfterClass
	public void CloseChrome() {
		driver.quit();
		
	}   

	public void espera(int espera) {
		try {Thread.sleep(espera);} catch (InterruptedException e) {e.printStackTrace();}
	}
	

	
	/*************************************************************/
	/****************    SELECCIONA USUARIO    *******************/
	/*************************************************************/ 
	
	
	
	
	String chooseYourUser ="it@opratel.com"; //Usuario Premium	
	//String chooseYourUser ="comprovidanueva@gmail.com"; //Usuario No Premium
	
	
	String passAmbiente ="123456"; //Contraseña QA it@opratel.com 
	//String passAmbiente ="t0g4.0pr4"; //Contraseña PRODUCCION
	//String passAmbiente ="tg*0pr4t31"; //Contraseña DV4
	
	
	
	
	/*************************************************************/
	/************   SELECCION A QUE AMBIENTE APUNTA  *************/
	/*************************************************************/ 
	
	String ambiente = "todosgamers.com";
	
	
	String apuntaA ="http://qa."; //AMBIENTE QA
	
	//String apuntaA ="http://dev4."; //AMBIENTE DEV7
	
	//String apuntaA ="http://"; //AMBIENTE PRODUCCION
	
	
	
	
	/*************************************************************/
	/*************************************************************/
	/*************************************************************/
	
 

	 
 	
	
	@Test (priority=0)
	public void LogIn() { 		
		EDPTG03LogIn TP = new EDPTG03LogIn(driver);
		TP.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP.LogOutTodosGamers(apuntaA);
	}
	     
	
	@Test (priority=1) 
	public void MiPerfil()	{
	EDPTG04MiPerfil TP1 = new EDPTG04MiPerfil(driver);
	TP1.LogInTodosGamers(passAmbiente,chooseYourUser); 
		TP1.MiPerfilTodosGamers(apuntaA);
		TP1.LogOutTodosGamers(apuntaA);
	}   
	
	
	
	@Test (priority=4)
	public void MisEquipos() {
		EDPTG07MisEquipos TP4 = new EDPTG07MisEquipos(driver);
		TP4.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP4.MisEquipos(apuntaA);
		TP4.LogOutTodosGamers(apuntaA);
	}
	
	
	@Test (priority=5)
	public void MisTorneos() {
	EDPTG08MisTorneos TP5 = new EDPTG08MisTorneos(driver);
		TP5.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP5.MisTorneos(apuntaA);
		TP5.LogOutTodosGamers(apuntaA);
	}
	
	
	@Test (priority=6)
	public void GenerarCodigoReferidos() {
	EDPTG09GenerarCodigoReferidos TP6 = new EDPTG09GenerarCodigoReferidos(driver);
	TP6.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP6.GenerarCodigoReferidos(apuntaA);
		TP6.LogOutTodosGamers(apuntaA);
	} 
	
	//Agregar EDPTG10UtilizarCodigoReferido
	
	@Test (priority=7)
	public void Inicio() {
	EDPTG11Inicio TP8 = new EDPTG11Inicio(driver);
		TP8.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP8.Inicio();
		TP8.LogOutTodosGamers(apuntaA);
	} 
	
	
	@Test (priority=8)
	public void Torneos() {
	EDPTG12Torneos TP9 = new EDPTG12Torneos(driver);
	TP9.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP9.Torneos();
		TP9.LogOutTodosGamers(apuntaA);
	} 
	
	
	@Test (priority=15)
	public void administrarPartida() {
	EDPTG21AdministrarPartidas TP16 = new EDPTG21AdministrarPartidas(driver);
	TP16.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP16.Torneos(apuntaA);
		TP16.administrar();
		TP16.LogOutTodosGamers(apuntaA);
	}
	
	
	@Test (priority=17)
	public void seccionEquipos() {
		EDPTG23Equipos TP18 = new EDPTG23Equipos(driver);
		TP18.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP18.Equipos(apuntaA);
		TP18.LogOutTodosGamers(apuntaA);
	}
	
	
	
	@Test (priority=22)
	public void misiones() {
	EDPTG28Misiones TP23 = new EDPTG28Misiones(driver);
		TP23.LogInTodosGamers(passAmbiente,chooseYourUser);
			TP23.Misiones();
			TP23.LogOutTodosGamers(apuntaA);
	}
	
	
	@Test (priority=26)
	public void rankingTG() {
	EDPTG33Ranking TP27 = new EDPTG33Ranking(driver);
	TP27.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP27.RankingSeccion();
		TP27.LogOutTodosGamers(apuntaA);
		}
	
	
	@Test (priority=28)
	public void CSGOLOBBY() {
	EDPTG35CSGOLOBBY TP29 = new EDPTG35CSGOLOBBY(driver);
	TP29.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP29.CSGOLOBBYSECCION(apuntaA);
		TP29.LogOutTodosGamers(apuntaA);
	}
	
	
	@Test (priority=31)
	public void noticias() {
	EDPTG38NOTICIAS TP32 = new EDPTG38NOTICIAS(driver);
	TP32.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP32.noticias();
		TP32.LogOutTodosGamers(apuntaA);
	}
	
	@Test (priority=32)
	public void noticiaspopulares() {
		EDPTG39NOTICIASPOPULARES TP33 = new EDPTG39NOTICIASPOPULARES(driver);
		TP33.LogInTodosGamers(passAmbiente,chooseYourUser);
			TP33.noticias();
			TP33.LogOutTodosGamers(apuntaA);
	}
	
	@Test (priority=33)
	public void noticiasrecientes() {
	EDPTG40NOTICIASRECIENTES TP34 = new EDPTG40NOTICIASRECIENTES(driver);
	TP34.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP34.noticias();
		TP34.LogOutTodosGamers(apuntaA);
	}
	
	
	@Test (priority=34)
	public void CMSTemporadasTG() {
	EDPTG41CMSTEMPORADAS TP35 = new EDPTG41CMSTEMPORADAS(driver);
	TP35.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP35.CMSTemporadas(apuntaA);
		TP35.LogOutTodosGamers(apuntaA);
	} 
	 
	
	@Test (priority=38)
	public void CMSAsociacionR() {
	EDPTG45CMSAsociacionRanking TP39 = new EDPTG45CMSAsociacionRanking(driver);
	TP39.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP39.CMSAsociacionRanking(apuntaA);
		TP39.LogOutTodosGamers(apuntaA);
	}
	
	@Test (priority=39)
	public void CMSLISTNOT() {
	EDPTG50CMSListNotification TP40 = new EDPTG50CMSListNotification(driver);
	TP40.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP40.CMSListNotification(apuntaA);
		TP40.LogOutTodosGamers(apuntaA);
	}
	
	@Test (priority=40)
	public void CMSNOTIFICACION() {
	EDPTG51CMSNotificacion TP41 = new EDPTG51CMSNotificacion(driver);
	TP41.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP41.CMSNotification(apuntaA);
		TP41.LogOutTodosGamers(apuntaA);
	}
	
	
	
	@Test (priority=42)
	public void CMSReqUsuario() {
	EDPTG53CMSReqUsuario TP43 = new EDPTG53CMSReqUsuario(driver);
	TP43.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP43.CMSRUsuario(apuntaA);
		TP43.LogOutTodosGamers(apuntaA);
	}
	
	@Test (priority=43)
	public void CMSLoginAsTest() {
	EDPTG54CMSLogInAs TP44 = new EDPTG54CMSLogInAs(driver);
	TP44.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP44.CMSLoginAs(apuntaA);
		TP44.LogOutTodosGamers(apuntaA);
	}
	
	@Test (priority=44)
	public void CMSPAYPALLSUS() {
	EDPTG55CMSPaypallSuscripcion TP45 = new EDPTG55CMSPaypallSuscripcion(driver);
	TP45.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP45.CMSPaypallSuscripcion(apuntaA);
		TP45.LogOutTodosGamers(apuntaA);
	}
	 
		 
	 
	@Test (priority=48)
	public void CMSLoadDashboard() {
	EDPTG59CMSCargarDashboard TP49 = new EDPTG59CMSCargarDashboard(driver);
		TP49.LogInTodosGamers(passAmbiente,chooseYourUser);
			TP49.CMSCD(apuntaA);
			TP49.LogOutTodosGamers(apuntaA);
	}
	
	
	@Test (priority=50)
	public void CMSNews() {
	EDPTG63CMSNoticias TP53 = new EDPTG63CMSNoticias(driver);
			TP53.LogInTodosGamers(passAmbiente,chooseYourUser);
				TP53.CMSNotocias(apuntaA);
				TP53.LogOutTodosGamers(apuntaA);
	}
	 
		
	@Test (priority=54)
	public void CMSGraceDayTest() {
	EDPTG67CMSGraceDay TP57 = new EDPTG67CMSGraceDay(driver);
		TP57.LogInTodosGamers(passAmbiente,chooseYourUser);
			TP57.CMSGraceDay(apuntaA);
			TP57.LogOutTodosGamers(apuntaA);
	}
	
	
	
	
	@Test (priority=57)
	public void CMSMENU() {
	EDPTG71CMSMenu TP60 = new EDPTG71CMSMenu(driver);
	TP60.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP60.CMSmenu(apuntaA);
		TP60.LogOutTodosGamers(apuntaA);
	} 
	

	
	//@Test (priority=60) ROMPE deben arreglar ruta de cms matchmaking
	public void CMSLobby() {
	EDPTG75CMSLobby TP63 = new EDPTG75CMSLobby(driver);
	TP63.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP63.CMSLobby();
		TP63.LogOutTodosGamers(apuntaA);
	}
	
	//@Test (priority=61) ROMPE deben arreglar ruta cms matchmaking
	public void CMSLobbyConsult() {
	EDPTG76CMSLobbyConsulta TP64 = new EDPTG76CMSLobbyConsulta(driver);
	TP64.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP64.CMSLobby();
		TP64.LogOutTodosGamers(apuntaA);
	}
	
	
	@Test (priority=62)
	public void CMSpermisos() {
	EDPTG77CMSPermisos TP65 = new EDPTG77CMSPermisos(driver);
	TP65.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP65.CMSPermisos(apuntaA);
		TP65.LogOutTodosGamers(apuntaA);
	}
	
	@Test (priority=66)
	public void CMSRolesTest() {
	EDPTG81CMSRoles TP69 = new EDPTG81CMSRoles(driver);
	TP69.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP69.CMSRoles(apuntaA);
		TP69.LogOutTodosGamers(apuntaA);
	}
	
 
	@Test (priority=69)
	public void CMSUsuarioTest() {
	EDPTG85CMSUsuarios TP72 = new EDPTG85CMSUsuarios(driver);
	TP72.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP72.CMSUsuario(apuntaA);
		TP72.LogOutTodosGamers(apuntaA);
	}

	
	@Test (priority=72)
	public void CMSTorneo() {
	EDPTG89CMSTorneos TP75 = new EDPTG89CMSTorneos(driver);
		TP75.LogInTodosGamers(passAmbiente,chooseYourUser);
			TP75.CMSTorneo(apuntaA);
			TP75.LogOutTodosGamers(apuntaA);
	}
	
	
	
	@Test (priority=78)
	public void CMSNuevaMIsion() {
	EDPTG95CMSNuevaMision TP81 = new EDPTG95CMSNuevaMision(driver);
	TP81.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP81.Misiones(apuntaA);
			TP81.LogOutTodosGamers(apuntaA);
	}
	 
	
	@Test (priority=81)
	public void CMSPremiosTest() {
		EDPTG99CMSPremios TP85 = new EDPTG99CMSPremios(driver);
	TP85.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP85.CMSPremios(apuntaA);
		TP85.LogOutTodosGamers(apuntaA);
	
	}
	
	
	@Test (priority=84)
	public void CMSTraduccionesTest() {
	EDPTG103CMSTraducciones TP87 = new EDPTG103CMSTraducciones(driver);
	TP87.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP87.CMSTraducciones(apuntaA);
		TP87.LogOutTodosGamers(apuntaA);
	}
	
	@Test (priority=85)
	public void CMSTutorialesTest() {
	EDPTG104CMSTutoriales TP88 = new EDPTG104CMSTutoriales(driver);
	TP88.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP88.CMSTutoriales(apuntaA);
		TP88.LogOutTodosGamers(apuntaA);
	}
	
	
	@Test (priority=89)
	public void LogOut() {
	EDPTG109LogOut TP92 = new EDPTG109LogOut(driver);
	TP92.LogInTodosGamers(passAmbiente,chooseYourUser);
	TP92.LogOutTodosGamers(apuntaA);
	}
	
	@Test (priority=90)
	public void idioma() {
	EDPTG108Lenguaje TP93 = new EDPTG108Lenguaje(driver);
	TP93.LogInTodosGamers(passAmbiente,chooseYourUser);
	TP93.lenguaje(apuntaA);
	TP93.LogOutTodosGamers(apuntaA); 
	}
	
	@Test (priority=91)
	public void OfflineTournaments() {
	EDPTG110DeslogueadoTournaments TP94 = new EDPTG110DeslogueadoTournaments(driver);
	TP94.TournamentsOffLine(apuntaA);
	
	}
	
	@Test (priority=92)
	public void OfflineChallenges() {
	EDPTG111DeslogueadoChallenges TP95 = new EDPTG111DeslogueadoChallenges(driver);
	TP95.ChallengesOffLine(apuntaA);
	
	}
	
	@Test (priority=93)
	public void OfflineRanking() {
	EDPTG112DeslogueadoRanking TP96 = new EDPTG112DeslogueadoRanking(driver);
	TP96.RankingOffLine(apuntaA);
	
	}
	
	@Test (priority=94)
	public void NoticiasRanking() {
	EDPTG113DeslogueadoNews TP97 = new EDPTG113DeslogueadoNews(driver);
	TP97.NoticiasOffLine(apuntaA);
	
	}
	
	
	
	
	
}
	
	
	/*Jenkins: http://54.163.143.224:8080/ 
	 		   http:qaas.opratel.com:8080
	 * Usuario: qaauttg
	 * Contraseña: t0g4.0pr4
	 * */
